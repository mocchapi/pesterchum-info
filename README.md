# Chapters
### Clients
#### [Clients](Clients.md)
### Protocol
#### [Protocol](Protocol.md)
#### [Compatibility](Compatibility.md)
#### [Numbers, constants, and other fun stuff](Numbers,%20constants,%20and%20other%20fun%20stuff.md)
### Themes
#### [Installing pesterchum themes](Installing%20pesterchum%20themes.md)
#### [Making pesterchum themes](Making%20pesterchum%20themes.md)
#### [Making pesterchum godot themes](Making%20pesterchum%20godot%20themes.md)

# Years
### [Pesterchum in 2023](Pesterchum%20in%202023.md)
<p xmlns:cc="http://creativecommons.org/ns#" xmlns:dct="http://purl.org/dc/terms/"><a property="dct:title" rel="cc:attributionURL" href="https://gitlab.com/mocchapi/pesterchum-info">Pesterchum info</a> by <span property="cc:attributionName">(lis)anne & contributors</span> is licensed under <a href="http://creativecommons.org/licenses/by-nc-sa/4.0/?ref=chooser-v1" target="_blank" rel="license noopener noreferrer" style="display:inline-block;">CC BY-NC-SA 4.0<img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/nc.svg?ref=chooser-v1"><img style="height:22px!important;margin-left:3px;vertical-align:text-bottom;" src="https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1"></a></p>