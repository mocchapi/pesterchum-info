This year:
### [Pesterchum-alt-servers](https://homestuck2.com/) gets a new release!
2.6 brings fixes for awesome out of date OS certificates & other fun stuff, alongside a bunch of new features merged for the next 2.7 release.
### [Pesterchum godot](https://gitlab.com/mocchapi/pesterchum-godot) gets a new release!
A slowdown from last year, but brings a bunch of things that really should've been added before. & then promptly becomes inactive again. oops
### The discord server suffers a coup d'état
Farewell democracy o7
### The [theme repository project](https://github.com/mocchapi/pesterchum-themes) continues to grow
It now has 49 themes, of which 4 were made this year. Pesterchum-alt-servers now also has built-in support for it, though it's completely broken :)
### "Having 2000 instances open" remains the #1 support problem
Hey what's a tray?
### As expected, the grand "3 new planned clients" from [last year](./Pesterchum in 2023.md) all failed to materialize
Scientists baffled