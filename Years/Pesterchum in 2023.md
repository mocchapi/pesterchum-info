This year:
### [Pesterchum-alt-servers](https://homestuck2.com/) gets over 150 new commits
Shou continues to ship-of-theseus it into the perfect software. A bunch of rad new features, including protocol additions, are set to ship<sup> get it</sup> for the mythical next release.
### [Pesterchum godot](https://gitlab.com/mocchapi/pesterchum-godot) gets 3 new releases
With the project being started in november 2022, it got 3 alpha releases taking it from "absolute garbage" to "kinda usable". Its now also distributed on itch.io.  
### [irc.pesterchum.xyz](https://pesterchum.xyz) migrates to UnrealIRCD 6
Despite a previous test with version 6 resulting in chumdroid breaking, it somehow still functions this time around.
### The [theme repository project](https://github.com/mocchapi/pesterchum-themes) is started
It has 29 themes, of which 21 were made this year.
### The [Pesterchum theme editor 2000](https://mocchapi.itch.io/pesterchum-theme-editor-2000) is created to help ease the theme making process
Critics call it "badly performing" and "confusing" and they are. correct.
### Random middle school students discover [pesterchum.online](https://pesterchum.online) is a chat program that's not blocked on their network
General chaos and moderation nightmares ensue. It also got its special set of extra rules.  
<sup>(( link approved by mods btw ))</sup>
### [HS^2](https://homestuck2.com/) gets a soft reboot, and one of the updates [features one of the clients](https://cohost.org/mocha/post/3300914-wtf-lisanne-referenc). sort of.
### A joke gets out of hand and three extra clients get planned
Only time will tell if the world will ever see pesterchum on nokia phones. or 3ds. or minecraft.