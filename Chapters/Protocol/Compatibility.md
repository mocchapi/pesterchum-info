As more new features get added to the pesterchum protocol, old/out-of-date/abandoned clients will drift further & further in compatibility. This page aims to list the recent/upcoming changes that will cause noticeable incorrect behavior in older clients (chumdroid, pesterchum 3.14, pre-pesterchum-alt-servers, etc)

# Metadata
IRCv3 Metadata plugin is unsupported by chumdroid and versions of the (original) desktop program older than pesterchum-alt-servers.
Implementing only metadata will cause these older clients to not register your mood or color.
### Workaround
To maintain compatibility with these older clients, you should still honor `GETMOOD` requests as seen in [Protocol](Protocol.md). This will only return mood interoperability. 
To also have colors register correctly, you must report your user color to the `#pesterchum` channel, as there is no mechanism for these older clients to request the color from you.

# `<alt>` Alternian font
the `<alt>` tag allows users to write text that gets displayed in the troll language *alternian* font. It works like this:  
`<alt>this text will appear in an alternian/deadric font</alt>`  
Alternian font tags are currently **only** supported by pesterchum-alt-server 2.6.0 and later. The impact if this is not supported is that instead of being rendered in an Alternian font, a users message will be displayed in plain text with `<alt>` tags around it. This seems acceptable and in some cases perhaps preferable.
### Workaround
No workaround is possible to make this work on out-of-date clients