# Moods
You can reference the [pesterchum-alt-servers Pesterchum theme](https://github.com/Dpeta/pesterchum-alt-servers/blob/main/themes/pesterchum/style.js#L84C9-L84C9) for what the mood icons should look like. The order **is important**, as they are exclusively referenced by index in the protocol.
0. `chummy`
1. `rancorous`
2. `offline`
3. `pleasant`
4. `distraught`
5. `pranky`
6. `smooth`
7. `ecstatic`
8. `relaxed`
9. `discontent`
10. `devious`
11. `sleek`
12. `detestful`
13. `mirthful`
14. `manipulative`
15. `vigorous`
16. `perky`
17. `acceptant`
18. `protective`
19. `mystified`
20. `amazed`
21. `insolent`
22. `bemused`
# Emotes
The common emote names & the image they should appear as. This list was taken & altered from the [pesterchum-alt-servers README](https://github.com/Dpeta/pesterchum-alt-servers#smilies-).

| name                   | image                                                                                                                                        |
| ---------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| `:rancorous:`          | ![:rancorous: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_rancorous.png)         |
| `:apple:`              | ![:apple: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/apple.png)                    |
| `:bathearst:`          | ![:bathearst: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/bathearst.png)            |
| `:cathearst:`          | ![:cathearst: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/cathearst.png)            |
| `:woeful:`             | ![:woeful: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_bemused.png)              |
| `:sorrow:`             | ![:sorrow: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/blacktear.png)               |
| `:pleasant:`           | ![:pleasant: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_pleasant.png)           |
| `:blueghost:`          | ![:blueghost: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/blueslimer.gif)           |
| `:slimer:`             | ![:slimer: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/slimer.gif)                  |
| `:candycorn:`          | ![:candycorn: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/candycorn.png)            |
| `:cheer:`              | ![:cheer: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/cheer.gif)                    |
| `:duhjohn:`            | ![:duhjohn: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/confusedjohn.gif)           |
| `:datrump:`            | ![:datrump: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/datrump.png)                |
| `:facepalm:`           | ![:facepalm: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/facepalm.png)              |
| `:bonk:`               | ![:bonk: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/headbonk.gif)                  |
| `:mspa:`               | ![:mspa: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/mspa_face.png)                 |
| `:gun:`                | ![:gun: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/mspa_reader.gif)                |
| `:cal:`                | ![:cal: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/lilcal.png)                     |
| `:amazedfirman:`       | ![:amazedfirman: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_amazedfirman.png)   |
| `:amazed:`             | ![:amazed: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_amazed.png)               |
| `:chummy:`             | ![:chummy: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_chummy.png)               |
| `:cool:`               | ![:cool: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pccool.png)                    |
| `:smooth:`             | ![:smooth: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pccool.png)                  |
| `:distraughtfirman:`   | ![:distraughtfirman: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_distraughtfirman.png) |
| `:distraught:`         | ![:distraught: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_distraught.png)       |
| `:insolent:`           | ![:insolent: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_insolent.png)           |
| `:bemused:`            | ![:bemused: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_bemused.png)             |
| `:3:`                  | ![:3: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pckitty.png)                      |
| `:mystified:`          | ![:mystified: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_mystified.png)         |
| `:pranky:`             | ![:pranky: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_pranky.png)               |
| `:tense:`              | ![:tense: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pc_tense.png)                 |
| `:record:`             | ![:record: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/record.gif)                  |
| `:squiddle:`           | ![:squiddle: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/squiddle.gif)              |
| `:tab:`                | ![:tab: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/tab.gif)                        |
| `:beetip:`             | ![:beetip: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/theprofessor.png)            |
| `:flipout:`            | ![:flipout: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/weasel.gif)                 |
| `:befuddled:`          | ![:befuddled: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/what.png)                 |
| `:pumpkin:`            | ![:pumpkin: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/whatpumpkin.png)            |
| `:trollcool:`          | ![:trollcool: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/trollcool.png)            |
| `:jadecry:`            | ![:jadecry: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/jadespritehead.gif)         |
| `:ecstatic:`           | ![:ecstatic: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/ecstatic.png)              |
| `:relaxed:`            | ![:relaxed: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/relaxed.png)                |
| `:discontent:`         | ![:discontent: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/discontent.png)          |
| `:devious:`            | ![:devious: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/devious.png)                |
| `:sleek:`              | ![:sleek: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/sleek.png)                    |
| `:detestful:`          | ![:detestful: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/detestful.png)            |
| `:mirthful:`           | ![:mirthful: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/mirthful.png)              |
| `:manipulative:`       | ![:manipulative: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/manipulative.png)      |
| `:vigorous:`           | ![:vigorous: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/vigorous.png)              |
| `:perky:`              | ![:perky: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/perky.png)                    |
| `:acceptant:`          | ![:acceptant: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/acceptant.png)            |
| `:olliesouty:`         | ![:olliesouty: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/olliesouty.gif)          |
| `:billiards:`          | ![:billiards: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/poolballS.gif)            |
| `:billiardslarge:`     | ![:billiardslarge: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/poolballL.gif)       |
| `:whatdidyoudo:`       | ![:whatdidyoudo: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/whatdidyoudo.gif)      |
| `:brocool:`            | ![:brocool: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/pcstrider.png)              |
| `:trollbro:`           | ![:trollbro: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/trollbro.png)              |
| `:playagame:`          | ![:playagame: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/saw.gif)                  |
| `:trollc00l:`          | ![:trollc00l: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/trollc00l.gif)            |
| `:suckers:`            | ![:suckers: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/Suckers.gif)                |
| `:scorpio:`            | ![:scorpio: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/scorpio.gif)                |
| `:shades:`             | ![:shades: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/shades.png)                  |
| `:honk:`               | ![:honk: pesterchum smilie/emote](https://raw.githubusercontent.com/Dpeta/pesterchum-alt-servers/main/smilies/honk.png)                      |

# The secret protocol IRC channel
The old method for exchanging metadata is done through the `#pesterchum` irc channel. This channel should generally be hidden from users, as it mostly looks like gibberish.

# Service & bot handles
These are the handles of official pesterchum-adjacent services & bots, as seen on the main `irc.pesterchum.xyz` server
- **randomEncounter**: The bot powering the random encounter functionality. See [Protocol](Protocol.md) for more info
- **calSprite**: Bot that allows streamlined reporting of other users, & checking which canon handles have been online for how long
- **ChanServ**: Standard IRC chanserv
- **NickServ**: Standard IRC nickserv

# IRCv3 metadata draft keys
- `mood`: mood index as seen in the [moods](#Moods) segment
- `color`: accepts same values as `COLOR>` messages