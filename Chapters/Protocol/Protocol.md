Pesterchum clients have a set of *mostly* agreed upon protocols which are used to make most of the pesterchum-specific features work over a normal IRC connection. This page is intended for people who want to create their own client, or those who are just interested in the inner workings of pesterchum.  
Knowledge of normal IRC protocol is assumed, which you can read more about [here](https://modern.ircdocs.horse/) if you are unfamiliar.


# User metadata
User metadata is shared through two avenues: a hidden-from-the-user IRC channel called `#pesterchum`, and through an IRCv3 server-side draft plugin called "Metadata".
Both methods provide a way to figure out what a specific users mood & text color is. 
If you are creating a new client, it is preferred you use the second method. Although if you have the time, an implementation of the channel method as well would improve compatibility with old clients.
## Method 1: `#pesterchum`
This channel is the original implementation & works across all pesterchum clients on any IRC server, and generally requires less client-side setup to get working. The major drawback is that it can be used to trace which user is speaking to which other user, and is thus a slight privacy concern. **The actual contents of the messages are still private.** It also relies on the \#pesterchum being always available & having the rights to speak in it.
### Colors
After joining the #pesterchum channel, & anytime the user changes their text color, their client should send the following in the channel as a normal `PRIVMSG`:
```
COLOR r,g,b
```
Where `r`,`g`, & `b` are RGB color values in the 0-255 range (unlike in chat messages, hex codes cannot be used). An example would be:
```
COLOR 255,0,0
```
This would tell other clients to display the user's text as pure red.
Note that unlike with moods, a client cannot request another client to re-send the user's color value.
### Moods
Moods are represented as a number, which map to the various icons. A lookup map can be found in [Numbers, constants, and other fun stuff](Numbers,%20constants,%20and%20other%20fun%20stuff.md).

To notify other clients of your mood, get the index number of the mood and send it as such in `peserchum`:
```
MOOD >n
```
where `n` is your number.
An example to set your mood as "perky" would be:
```
MOOD >16
```

To figure out another user's mood, you can use the GETMOOD command, which (if compliant) will cause the client(s) of the user(s) mentioned to send a `MOOD >` of their own.
Issuing a GETMOOD is straight forward:
```
GETMOOD username1 username2 username3
```
If your user's handle is listed in one of these, your client should respond with a `MOOD >`. 
**Note:** pesterchum-alt-servers sends GETMOODS without spaces between names (IE: `GETMOOD username1username2`). Because of this, you cannot depend on the space as a delimiting character when detecting if your handle got mentioned.
## Method 2: Metadata IRCv3 plugin
[⚠️ This feature has compatibility implications](Compatibility.md)  
This method uses the IRCv3 "metadata" draft protocol, which you can read more about [here](https://gist.github.com/k4bek4be/92c2937cefd49990fbebd001faf2b237)
It does not have any of the problems of the channel method, and is generally preferred if you are creating a new client. The biggest drawback is that this requires the server to have [this plugin](https://github.com/pirc-pl/unrealircd-modules#metadata) (or another plugin that follows the spec).
Another caveat is that it requires your IRC library to support IRCv3 CAP handshakes, & you have to explicitly accept the metadata protocol. This should not be an issue for most, but if you are using very old implementations (or making your own) you will have to deal with this.

**Note:** IRCv3 metadata is a *draft*, meaning a future version is likely to differ from the current version. It also means its not officially part of IRCv3 specs yet. There's been some activity around it, as seen [here](https://github.com/ircv3/ircv3-specifications/pull/501), but it's unknown what the status of it all is, especially since metadata has been in the works for many years by various authors.

### Colors
### Moods
-----------


# Message formatting
## Colors
Default colors are set per-user as seen in the user metadata section, but a message itself can also contain any range of colors in addition to that. 
Inline colors are set by enclosing your text with `<c=>` & ``</c>`` html-like tags.
Note that transparency is broadly not supported (although incorrectly formed hex codes, with two extra trailing characters for alpha transparency, may render correctly on some clients. This **will** however render incorrectly on other clients)
The color *value* (what comes after the `c=` & determines the color) can be formatted in various ways:
### Hex code
HTML hex colors, where two alphanumeric characters are used for the red, green, & blue color channels.
```
<c=#008000>This text is green :)</c>
```
### RGB
0-256 RGB values. This method is most used by pesterchum-alt-servers & its ancestors, as well as chumdroid.
```
<c=0,128,0>This text is green :)</c>
```
### HTML name
[List of names here](https://www.w3schools.com/tags/ref_colornames.asp)  
Technically supported on *seemingly* all clients, but no client uses it as their preferred method of communicating colors. No guarantees as to which color names work on which client/platform, but the original 16 HTML colors are likely to work. You are discouraged from using this method for its general unreliability & lack of specificity.
```
<c=green>This color is green :)</c>
```
## /me

## Alternian font
[⚠️ This feature has compatibility implications](Compatibility.md)  
The inline `<alt>` tag can be used to render the enclosed text with a homestuck alternian font. 
```
This text will render using a normal font, but <alt>this text will render in an alternian font</alt>
```
You may want to add a method to display the text in a normal font when implementing this.

Here's a couple fonts you can (maybe) use in a client, but the truth is that they're all a bit of a license nightmare. Any Elder Scrolls Daedric font will do, of which many exist that aren't listed here, so pick your poison I guess.
- [Alli’s Daedric Font](https://www.fontspace.com/allis-daedric-font-f20263) by *AlliStarlo Designs* (Public domain, but daedric, so, yknow. bethesda) (used by pesterchum-alt-servers)
- [TrollType fonts](https://trolltype.mspa.sudrien.net/) by *Sudrien* & *Cyanide* (No redistribution)
- [Hiveswap alternian font](https://www.deviantart.com/orangeypeels/art/Hiveswap-Alternian-Font-704876917) by *OrangeyPeels* (Dubious licensing, seemingly taken from official HS art)

-----------


# Special action keys
Special keys can be used to signal to a client that the user has done a certain action.
These formed like `PESTERCHUM:ACTIONNAME`, with no initials prefix or color tags.
Heres a list of which exist, and how they should be shown to the user.
## `PESTERCHUM:BEGIN`
> -- initiatingUser [IU] started pestering receivingUser [RU] at HH:MM --  

**This is sent by a client when contacting another client through direct messaging.** This should cause the receiving user to get a new chat window/tab/whatever to let them know the sender initiated a conversation with them.

## `PESTERCHUM:BLOCK`
> --
------------


# Memos & time travel
IRC channels, called "memos" in clients, also have some extra sugar applied. Mimicking the webcomic, users in a memo can appear to be sending messages from a specific time in the past or future. This is purely a visual mechanic, mostly intended for RP. It is done by sending a hidden PRIVMSG to the channel. A user can set their timeframe multiple times, with the latest change being the "current" time of the user. A user who's time is set in the future should have `F` prefixed to their initials, the same being true for times set in the past with `P` and `C` for "current"/unset time .  
A client should track the order in which other users have set their times, and assign a number to each in that order. It should then add that number as a suffix to the user's initials when they are talking from that time, like this example log:

```
CURRENT exampleUser [CEU] RIGHT NOW responded to memo.
CEU: hello o/

PESTERCHUM:TIME>F01:00
FUTURE exampleUser [FEU] 1:00 HOUR FROM NOW responded to memo.
FEU: im in the future

PESTERCHUM:TIME>F02:00
FUTURE exampleUser [FEU2] 2:00 HOURS FROM NOW responded to memo.
FEU2: im even more in the future!! note the 2!

PESTERCHUM:TIME>F01:30
FUTURE exampleUser [FEU3] 1:30 HOURS FROM NOW responded to memo.
FEU3: note that the numbering is from the order of appearance! not the order in time!

PESTERCHUM:TIME>P00:30
PAST exampleUser [PEU] 30 MINUTES AGO responded to memo.
PEU: it works the same way for Past timeframes, but it has a seperate set of numbers
```

Note that timeframes are a memo-only feature! This syntax is not supported by any client within direct message conversations.
## Setting time
This is done through the `PESTERCHUM:TIME>` message, which should be hidden from users. `PESTERCHUM:TIME>` should be followed by either the character `P`/`F` + a time, or simply `i`. 

Set user timeframe to two hours and one minute in the past (P for past)
```
PESTERCHUM:TIME>P02:01
```

Set user timeframe to one hour in the future (F for future)
```
PESTERCHUM:TIME>F01:00
```

Reset user timeframe to RIGHT NOW/current time (i for *initial*)
```
PESTERCHUM:TIME>i
```
Note that time values do not have to be constrained to two sets of two characters, `PESTERCHUM:TIME>F100:9999` is considered valid by most clients. Leading zeroes are also optional.

-------------------


# Differences between direct messaging & memos

-----


# Random encounters
Random encounters are not strictly part of the protocol, as it depends on the external [randomEncounter](https://github.com/Dpeta/randomEncounter) bot.
Random encounters are a mechanism that allows users who wish to start a conversation with another random & willing user. It is often used for initiating roleplays.
Because it involves unexpectedly & possibly often popping up a new chat window there is a way to opt out on a per-nickname basis.
Both for finding a willing participant as well as opting out of random conversations, the `randomEnounter` bot is used. This keeps track of all of those who have opted out, & will select a random user not in that list upon request.
The IRC NOTICEs listed below should be sent to the `randomEncounter` user on the official irc.pesterchum.xyz server, but other servers could potentially have it under a different nick.

**Note:** randomEncounter sets all users to allow to be encountered by default. Your client should communicate the users preference as soon as it connects to a server, or as soon as the randomEncounter bot comes online.

From that same repository's [README.md](https://github.com/Dpeta/randomEncounter/blob/main/README.md#protocol):
>- Responds to `NOTICE !` with `NOTICE !=randomUser` , the notice is what Pesterchum uses to do random encounters. (Pesterchum user clicked "Random Encounter" )
>
>- Responds to `NOTICE -` with `NOTICE -=k`, the sender is excluded from further random encounters. (User turned off random encounters.)
>
>- Responds to `NOTICE +` with `NOTICE +=k`, the sender is removed from the exclude list if present. (User turned on random encounters.)
>
>- Responds to `NOTICE ~` with `NOTICE ~=k`, the sender is excluded from further random encounters. (User is AFK.)
>
>- Responds to `NOTICE +` with `NOTICE +=k`, the sender is removed from the idle exclude list if present. (User is no longer AFK.)
>
>- Responds to all incoming `PRIVMSG` with a random user.
>
>- Responds to `PRIVMSG #pesterchum GETMOOD randomEncounter` with `PRIVMSG #pesterchum MOOD >18`.

This boils down to:
- send `NOTICE -` to exclude us from getting randomly encountered
- send `NOTICE +` to allow other users to randomly encounter us
- send `NOTICE !` to get a random nickname to encounter
- use `NOTICE ~`  if your client tracks user idle state.