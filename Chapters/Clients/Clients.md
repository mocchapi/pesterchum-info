There exist several programs to use the pesterchum network, a general term for which is "client". They exist for *most* platforms, like all major operating systems, web browsers, & phones. Some work with the common protocol over IRC, and some are too old/use their own systems.

# Modern/compatible clients
Clients that all use IRC & mostly share the same protocol as listed in the protocol chapters. These all currently work on a base level, but some more than others
## Pesterchum (pyqt) (windows, macos, linux)
The original, still being actively worked on since 2011. Originally GhostDunk's invention, who also ran the original servers. This version is the most well-known & feature rich. For several years its been developed by Dpeta/Shou as pesterchum-alt-servers (since they also run the pesterchum.xyz server) (ty shou).
- Themes
- Random encounters
- Chumrolls
- Chat histories
It is licensed under GPLv3
## [Pesterchum godot](https://gitlab.com/mocchapi/pesterchum-godot) (android, web, windows, macos, linux)
Mostly intended as a modern Chumdroid alternative, it runs on most platforms except IOS.  
its developed by me :)  
It is licensed under GPLv3  
## [Pesterchum.online](https://pesterchum.online) (web)
Probably the most accessible client, this is a pure JS web client you can use in your browser. It requires no downloads or specific supporting backend, as it uses websockets.
Developed by Dpeta & laaledesiempre  
Its licensed under the AGPL  
## [Chumdroid]() (android)
Absolutely ANCIENT client for android. It has not seen an update in 10 ish years, and has many bugs. Somehow still semi-works though.  
It was updated to work with the new server, as the original only connects to the defunct ghostdunk server.  
You can download this version from the [pesterchum.xyz](https://pesterchum.xyz) website.  
Its author appears to be "m85" and seems to have existed on the "android market" (playstore before playstore) many years ago.  
Its license is unkown, althought its likely it has none/proprietary due to public source code having been found.  

# Old/incompatible clients
Clients that are either so old they are not in use/work anymore, or clients that decided to do their own spin on things and don't make use of the IRC/common protocol.
## [Pesterchum](https://github.com/timstableford/pesterchum) (java) (platforms unknown)
Little is known about this one. It's development seemingly ended in 2013. It's written in java and uses a custom server & protocol. I'll sift through the source code & figure some more stuff out eventually.  
It has no license.